<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 09.05.2018
 * Time: 18:20
 */

namespace app\helpers;

class Email
{

    public static function sendMailAdmin($subject, $text, $to, $from, $link)
    {
        $message = ' 
            <html> 
                <head> 
                    <title>Tasker - Обратная связь</title> 
                </head> 
                <body> 
                    <h1>Здравствуйте, вам пришло сообщение от пользователя </h1>
                    <a href="'.$link.'">Проверьте почту на странице администратора</a>
                </body> 
            </html>';

        $headers  = "Content-type: text/html; charset=UTF-8 \r\n";
        $headers .= "From:".$from."\r\n";
        $headers .= "Bcc: ".$to."\r\n";

       return mail($to, $subject, $message, $headers);
    }

    public static function sendMailUser($subject, $text, $to, $from, $link, $name)
    {
        $message = ' 
            <html> 
                <head> 
                    <title>Tasker - Обратная связь</title> 
                </head> 
                <body> 
                    <h1>Здравствуйте, '.ucfirst($name).' вы прислали сообщение через обратную связь сайта Tasker </h1>
                    <h4>Ответ:</h4>
                    <p>'.ucfirst($text).'</p> 
                    <a href="'.$link.'">Перейти на Tasker</a>
                </body> 
            </html>';

        $headers  = "Content-type: text/html; charset=UTF-8 \r\n";
        $headers .= "From:".$from."\r\n";
        $headers .= "Bcc: ".$to."\r\n";

       return mail($to, $subject, $message, $headers);
    }

}