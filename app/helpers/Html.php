<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 05.05.2018
 * Time: 18:18
 */

namespace app\helpers;

class Html
{
    /*convertTime
     * function converts unix time to user-friendly view
     *
     * */
    public static function convertTime($unix, $format = 'Y-m-d H:i')
    {
        return self::encode(date( $format, $unix));
    }

    /*encode
     * function convert symbols to html entities
     *
     */
    public static function encode($string)
    {
        return htmlspecialchars($string, ENT_QUOTES | ENT_SUBSTITUTE,  'UTF-8', true);
    }

    /*decode
     * function convert html entities to symbols
     *
     */
    public static function decode($string)
    {
        return htmlspecialchars_decode($string, ENT_QUOTES);
    }
}