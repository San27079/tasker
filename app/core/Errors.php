<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 11:11
 */
namespace app\core;

use app\core\Controller;

class Errors extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function actionPageNotFound()
    {
        $data['title'] = 'Tasker - Ошибка';
        $this->view->viewRender('errors/page_not_found', 'layouts/error_header', true, $data);
    }

}