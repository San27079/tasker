<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 05.05.2018
 * Time: 19:29
 */

namespace app\core;

class Settings
{

    private static $keys = [];

    private static function openJson()
    {
        $file = file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/config/keys.json');
        self::$keys = json_decode($file, true);
    }

    public static function getKey($name)
    {
        if(empty(self::$keys)){
            self::openJson();
        };
        if(isset(self::$keys[strtolower($name)])){
            return self::$keys[strtolower($name)];
        }else{
            return false;
        }
    }
}