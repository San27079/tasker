<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 05.05.2018
 * Time: 23:09
 */

$aliases = [
    ['original' => app\core\Settings::class, 'new' => 'Settings'],
    ['original' => app\helpers\Html::class, 'new' => 'Html'],
];

foreach ($aliases as $alias) {
    class_alias($alias['original'], $alias['new'], true);
}