<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 10:30
 */
namespace app\core;
require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/core/Aliases.php';

class Router
{
    public function __construct()
    {
        $this->errors = new Errors();

        $url = !empty($_GET["url"]) ? $_GET['url'] : null;

        if(empty($url)){
            $url[0] = 'index';
        }else {
            $url = explode('/', trim($url, '/'));
        };

        $controller_file = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/controllers/'.strtolower($url[0]).'.php';
        if(file_exists($controller_file)){
            $controller_name = strtolower($url[0]);
        }else{
            $this->errors->actionPageNotFound();
            exit;
        };
        $controller_name_namespace = 'app\\controllers\\'.$controller_name;
        $controller = new $controller_name_namespace;

        $controller->loadModel($controller_name);

        $parameters = array();
        if(!empty($url[1]) && !empty($url[2])){
            $action_name = 'action'.ucfirst($url[1]);
            $parameters = array_slice($url, 2);
        }else if(!empty($url[1])){
            $action_name = 'action'.ucfirst($url[1]);
        }else{
            $action_name = 'actionIndex';
        };

        if(method_exists($controller, $action_name)){
            $controller->$action_name($parameters);
        }else{
            $this->errors->actionPageNotFound();
            exit();
        };
    }
}