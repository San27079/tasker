<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 11:13
 */
namespace app\core;

class View
{
    public function __construct()
    {

    }

    function viewRender($path, $header = true, $footer = true, $data =[]){
        empty($data['title']) ? $data['title'] = 'Tasker' : $data['title'];
        empty($data['scripts']) ? $data['scripts'] = '' : $data['scripts'];
        extract($data);
        if($header === true){
            $header ? require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/views/layouts/header.php': '';
        }elseif (is_string($header)){
            require $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/views/'.$header.'.php';
        }
        require $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/views/'.$path.'.php';
        if($footer === true){
            $footer ? require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/views/layouts/footer.php': '';
        }elseif (is_string($footer)){
            require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/views/'.$footer.'.php';
        }
        return true;
    }
}