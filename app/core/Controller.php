<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 11:25
 */
namespace app\core;

class Controller
{
    public function __construct()
    {
        $this->view = new View();
        $this->model = new Model();
        $this->storage = new Storage();
        session_start();
    }

    public function loadModel($model){
        $model_file =  $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/models/'.ucfirst($model).'Model.php';
        if(file_exists($model_file)){
            $model_class = 'app\\models\\'.ucfirst($model).'Model';
            $this->model = new $model_class();
        }else{
            return;
        };
    }

    protected function loadOtherModel($model){
        $model_file =  $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/models/'.ucfirst($model).'Model.php';
        if(file_exists($model_file)){
            $model_class = 'app\\models\\'.ucfirst($model).'Model';
            $this->$model = new $model_class();
        }else{
            return;
        };
    }

    protected function checkAuthUser(){
        $id = $_SESSION['id'];
        $hash = $_SESSION['hash'];
        if(!empty($id) && !empty($hash)) {
            if (!$this->model->checkHashUsers($id, $hash)) {
                $this->destroySessionData();
                header('Location: '.$this->storage->getData('domain').'/index/login', true);
            }else{
                return true;
            }
        }else{
            $this->destroySessionData();
            header('Location:'.$this->storage->getData('domain') .'/index/login', true);
        }
    }

    protected function checkAuthAdmin(){

        $id = $_SESSION['id_admin'];
        $hash = $_SESSION['hash_admin'];
        if(!empty($id) && !empty($hash)) {
            if (!$this->model->checkHashUsers($id, $hash, true)) {
                $this->destroySessionDataAdmin();
                header('Location: '.$this->storage->getData('domain') .'/admin/login', true);
            }else{
                return;
            }
        }else{
            $this->destroySessionDataAdmin();
            header('Location: '.$this->storage->getData('domain') .'/admin/login', true);
        }
    }
    protected static function createImage($image, $width)
    {
        $image_params = getimagesize($image);
        $height = ceil($width/($image_params[0]/$image_params[1]));
        $format = exif_imagetype($image);
        $new_image = imagecreatetruecolor($width, $height);
        switch ($format) {
            case IMAGETYPE_GIF:
                $get_image = imagecreatefromgif($image);
                imagecopyresampled($new_image, $get_image,0,0,0,0,$width,$height,imagesx($get_image),imagesy($get_image));
                imagegif($new_image, $image);
                imagedestroy($get_image);
                break;
            case IMAGETYPE_PNG:
                $quality = 2;
                $get_image = imagecreatefrompng($image);
                imagecopyresampled($new_image, $get_image,0,0,0,0,$width,$height,imagesx($get_image),imagesy($get_image));
                imagepng($new_image, $image ,$quality);
                imagedestroy($get_image);
                break;
            default:
                $quality = 100;
                $get_image = imagecreatefromjpeg($image);
                imagecopyresampled($new_image, $get_image,0,0,0,0,$width,$height,imagesx($get_image),imagesy($get_image));
                imagejpeg($new_image, $image ,$quality);
                imagedestroy($get_image);
                break;
        };
        imagedestroy($new_image);
    }

    protected function destroySessionData(){
        unset($_SESSION['id']);
        unset($_SESSION['hash']);
        unset($_SESSION['name']);
        session_destroy();
    }

    protected function destroySessionDataAdmin(){
        unset($_SESSION['id_admin']);
        unset($_SESSION['hash_admin']);
        unset($_SESSION['name_admin']);
        session_destroy();
    }
}