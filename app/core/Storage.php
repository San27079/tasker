<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 05.05.2018
 * Time: 17:13
 */

namespace app\core;


class Storage
{

    protected $Data =
        [
            'colors_ban' => [
                0 => 'text-success',
                1 => 'text-warning',
                2 => 'text-danger'
            ],
            'ban_stats' => [
                0 => 'Бана нет',
                1 => 'Забанен на срок',
                2 => 'Забанен навечно'
            ],
            'colors_stats' => [
                0 => 'text-primary',
                1 => 'text-warning',
                2 => 'text-success'
            ],
            'statuses' => [
                0 => 'Новое',
                1 => 'В процесссе',
                2 => 'Завершено'
            ],
            'domain' => 'http://tasker',
            'email_admin' => 'sano_admin@my-tasker.zzz.com.ua'
        ];

    public function getData($var)
    {
        if(isset($this->Data[strtolower($var)])){
            return $this->Data[strtolower($var)];
        }else{
            return false;
        }
    }

    public function setData($var_name, $value)
    {
        if(!isset($this->Data[strtolower($var_name)])){
            $this->Data[strtolower($var_name)] = $value;
            return true;
        }else{
            return false;
        }
    }

}