<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 12:00
 */
namespace app\core;


class Model
{
    public function __construct()
    {
        $this->db = new Db();
    }

    public function checkHashUsers($id, $hash, $admin = false)
    {
        if(!$admin) {
            $sql = "SELECT hash FROM users WHERE id=:id";
        }else{
            $sql = "SELECT hash FROM tasker_admins WHERE id=:id";
        }
        $finded_hash = $this->db->prepare($sql);
        $finded_hash->execute(array(':id' => $id));
        $finded_hash = $finded_hash->fetch();
        if($hash === $finded_hash['hash']){
            return true;
        }else{
            return false;
        }
    }

    public function getTask($id)
    {
        $sql = "SELECT * FROM tasks WHERE id=:id";
        $task = $this->db->prepare($sql);
        $task->execute(array(':id' => $id));
        $task = $task->fetch();
        return $task;
    }

    public function deleteTask($id)
    {
        $delete = $this->getTask($id);
        if($delete['image'] != '/public/img/tasks/default.jpg') {
            $delete = substr($delete['image'], 1);
            unlink($delete);
        };
        $sql = "DELETE FROM tasks WHERE id=:id";
        $task = $this->db->prepare($sql);
        $task->execute(array(':id' => $id));
        return true;
    }

    public function updateTask($id, $text, $stat, $title)
    {
        $sql = "UPDATE tasks SET stat =:status, task_text=:text, title=:title WHERE id=:id";
        $task = $this->db->prepare($sql);
        $task->execute(array(':id' => $id, ':text' => $text, ':status' => $stat, ':title' => $title));
        return true;
    }

    public function getAllHistoryTasks($id)
    {
        $sql = 'SELECT * FROM tasks WHERE user_id=:id AND history = 1 ORDER BY date_create  ASC';
        $tasks = $this->db->prepare($sql);
        $tasks->execute(array(':id' => $id));
        $tasks = $tasks->fetchAll();
        return $tasks;
    }

    public function checkReCaptha($recaptha)
    {
        $secret =  Settings::getKey('g_secret_key');
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$recaptha";
        if(!empty($recaptha)){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $json = curl_exec($ch);
            $output = json_decode($json, true);
            curl_close($ch);
            if($output['success']){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }


}