<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 12:00
 */
namespace app\core;

class Db extends \PDO
{
    public function __construct()
    {
        $db_config = file_get_contents($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'app/config/db.json');
        $db_config = json_decode($db_config, true);
        $dsn = 'mysql:host='.$db_config['host'].'; dbname='.$db_config["db_name"];
        parent::__construct($dsn, $db_config['user_name'], $db_config['password']);
    }
}