<?php if(empty($tasks)):?>
    <div class="container">
        <h1 class="text-secondary display-3 padding-top-login">
            Пока еще нет заданий, но вы можете их добавить
        </h1>
        <p class="text-secondary text-center">Чтобы добавить новую задачу <a href="/index/create">Создать задачу</a></p>
    </div>
<?php else: ?>
    <div class="container">
        <div class="row clearfix d-flex justify-content-center justify-content-lg-between">
            <?php foreach ($tasks as $task):?>
                <div class="card background-card-style margin-card-top" style="background-image: url('<?=Html::encode($task['image'])?>')">
                    <div class="card-body margin-top-history">
                        <h5><?=Html::encode($task['title'])?></h5>
                        <div class="scroll_content">
                            <ul class="list-group">
                                <?php
                                    foreach (explode(PHP_EOL, $task['task_text']) as $li){
                                        echo "<li class='list-group-item'>".Html::encode($li)."</li>";
                                }?>
                            </ul>
                        </div>
                        <div class="bottom-buttons">
                            <a href="/index/Change/<?=$task['id']?>" class="btn btn-primary"><i class="far fa-edit"></i></a>
                            <?php if($task['stat'] != 2):?>
                                <a href="/index/ChangeStatus/<?=$task['id']?>/<?=$task['stat']+1?>" class="btn btn-success"><i class="fas fa-thermometer-quarter"></i></a>
                            <?php else:?>
                                <a href="/index/ToHistory/<?=$task['id']?>" class="btn btn-success"><i class="fas fa-history"></i></a>
                            <?php endif;?>
                            <a href="/index/Delete/<?=$task['id']?>" class="btn btn-danger"><i class="fas fa-times"></i></a>
                            <span class="margin-left-15">
                                <?php if($task['stat'] == 0){?>
                                    <span class = "h5 text-primary">Новое</span>
                                <?php }elseif($task['stat'] == 1){ ?>
                                    <span class = "h5 text-warning">В процессе</span>
                                <?php }else{?>
                                    <span class = "h5 text-success">Выполнено</span>
                                <?php }?>
                            </span>
                            <p class="margin-bottom-0">
                                <span class="small"><?=(empty($task['change_date']))?'Дата создания: '.Html::convertTime($task['date_create']): 'Изменено: '.Html::convertTime($task['change_date'])?></span>
                                <a class="margin-left-15 notes-links" href="" data-note="<?=Html::encode($task['id'])?>">Заметки</a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <div class = "container margin-top-history">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="/index/index/<?= $_SESSION['sort_type'].'/'.(($active_page > 0)?($active_page-1): $active_page)?>">Предыдущая</a></li>
                <?php for($i = 0; $i < $count; $i++){ ?>
                    <li class="page-item <?php if($i == $active_page){echo 'active';}?>"><a class="page-link" href="/index/index/<?= $_SESSION['sort_type'].'/'.$i?>"><?= $i+1 ?></a></li>
                <?php };?>
                <li class="page-item"><a class="page-link" href="/index/index/<?= $_SESSION['sort_type'].'/'.(($active_page < $count-1)?($active_page+1): $active_page)?>">Следующая</a></li>
            </ul>
        </nav>
    </div>
<?php endif; ?>

<div class="modal fade" id="notesModal" tabindex="-1" role="dialog" aria-labelledby="notesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="notesModalLabel"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="notes-body">
            </div>
            <div class="modal-body">
                <div id="form-note"></div>
                <div id="error-note"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary button-close" data-dismiss="modal">Закрыть</button>
                <button class="btn btn-success" id="open-form">Добавить заметку</button>
            </div>
        </div>
    </div>
</div>

