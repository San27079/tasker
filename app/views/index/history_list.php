<div class="container margin-top-history">
    <div class="row">
        <div class="col-12">
            <h2>История завершенных событий</h2>
        </div>
        <br>
        <div class="col-lg-12">
            <form action="/index/History" class="form-inline" method="post">
                <div class="form-group">
                    <input type="text" name="word" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Найти" class="btn btn-primary">
                </div>
            </form>
        </div>
        <div class="col-12 margin-top-history">
            <?php if(empty($tasks)):?>
            <h2 class="text-secondary">Событий истории не найдено</h2>
            <?php else: ?>
            <ul class="list-group">
                <table class="table table-responsive-lg">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Заголовок</th>
                        <th scope="col">Задание</th>
                        <th scope="col">Дата выполнения</th>
                        <th scope="col">Статус</th>
                        <th scope="col">Операции</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($tasks as $task):?>
                        <tr>
                            <th scope="row"><?= Html::encode($task['id'])?></th>
                            <td><?= Html::encode($task['title'])?></td>
                            <td><?= Html::encode($task['task_text'])?></td>
                            <td><?=Html::convertTime($task['end_date'])?></td>
                            <td>
                                <?php if($task['stat'] == 0){?>
                                    <p class = "h5 text-primary">Новое</p>
                                <?php }elseif($task['stat'] == 1){ ?>
                                    <p class = "h5 text-warning">В процессе</p>
                                <?php }else{?>
                                    <p class = "h5 text-success">Выполнено</p>
                                <?php }?>
                            </td>
                            <td>
                                <a href="" data-id-task="<?=Html::encode($task['id'])?>"  class="btn btn-primary history-task-overview"><i class="fa fa-eye"></i></a>
                                <a href="/index/Delete/<?=Html::encode($task['id'])?>" class="btn btn-danger"><i class="fas fa-times"></i></a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            <?php endif;?>
        </div>
    </div>
</div>

<div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="historyModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="historyModalLabel">Просмотр события истории</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="card background-card-style" id="history-card-prev">
                            <div class="card-body margin-top-history">
                                <h4 id="preview-title"></h4>
                                <ul class="list-group" id="preview-text">
                                </ul>
                                <div class="bottom-buttons">
                                    <a href="#" class="btn btn-primary disabled"><i class="far fa-edit"></i></a>
                                    <a href="#" class="btn btn-success disabled"><i class="fas fa-history"></i></a>
                                    <a href="#" class="btn btn-danger disabled"><i class="fas fa-times"></i></a>
                                    <span class="margin-left-15"><span class = "h5 text-success">Завершено</span></span>
                                    <p class="margin-bottom-0 small" id="create_date"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <h4 class="text-center">Заметки</h4>
                            <hr>
                            <div id="notes-list">

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary button-close" data-dismiss="modal">Закрыть</button>
                <a href="" class="btn btn-danger" id="delete-item"> Удалить задачу</a>
            </div>
        </div>
    </div>
</div>

