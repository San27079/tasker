<div class="container">
    <h1 class = "text-center">Создать новую задачу:</h1>
    <div class="container d-flex justify-content-center main_page">
        <form action="/index/Create" method="post" class="my-form" enctype=multipart/form-data>
            <div class="form-group">
                <label  for = "title">Заголовок *</label>
                <input class="form-control" name="title" id = "title" placeholder="Введите название задачи">
            </div>
            <div class="form-group">
                <label  for = "task-text">Текст задачи *</label>
                <textarea class="form-control" name="task-text" id = "text" rows="6" placeholder="Введите текст задачи 5 - 600 символов. Подзадачи перечисляйте через Enter">
                </textarea>
            </div>
            <div class="form-group">
                <label for="image">Загрузка изображения</label>
                <input type="file" class="form-control-file" name="image" id="image" aria-describedby="image-help">
                <small id="image-help" class="form-text text-muted">Вы можете использовать изображения размером до 640 x 480 px. Поддерживаемые форматы JPG/GIF/PNG</small>
            </div>
            <input type="submit"  value="Создать" id="submit" class="btn btn-success">
            <button class="btn btn-primary" id="preview-button" data-status = "1" type="button">Предпросмотр</button>
        </form>
    </div>
    <p class = "text-danger"><?php if(!empty($errors)){echo Html::encode($errors);}?></p>
</div>
<div class="container">
    <div class="row d-lg-flex justify-content-lg-center">
        <div class="card background-card-style" id="preview-card">
            <div class="card-body margin-top-history">
                <h5 id="preview-title"></h5>
                <ul class="list-group" id="preview-text">
                </ul>
                <div class="bottom-buttons">
                    <a href="#" class="btn btn-primary disabled"><i class="far fa-edit"></i></a>
                    <a href="#" class="btn btn-success disabled"><i class="fas fa-history"></i></a>
                    <a href="#" class="btn btn-danger disabled"><i class="fas fa-times"></i></a>
                    <span class="margin-left-15">
                    <span class = "h5 text-primary">Новое</span></span>
                    <p class="margin-bottom-0 small" id="create_date">Дата создания: </p>
                </div>
            </div>
        </div>
    </div>
</div>

