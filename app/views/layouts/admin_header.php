<!doctype html>
<html lang=ru>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title; ?></title>
    <link rel="stylesheet" href="/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/css/style.css">
    <link rel="stylesheet" href="/public/css/fontawesome-all.css">
</head>
<body class="body-background-all">
<div class="main-div">
    <header>
        <nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="/admin/Index">
                    Tasker@<span class="small">Панель администратора</span>
                </a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item margin-right-15">
                            <span class="navbar-text">Добро пожаловать: <?= htmlspecialchars($_SESSION['name_admin'])?></span>
                        </li>
                        <li class="nav-item">
                            <a class="page-link" href="/admin/Index">Все события</a>
                        </li>
                        <li class="nav-item">
                            <a class="page-link" href="/admin/Users">Пользователи</a>
                        </li>
                        <li class="nav-item">
                            <a class="page-link" href="/admin/Mail">Почта</a>
                        </li>
                        <li class="nav-item">
                            <a class="page-link" href="/admin/Logout">Выход</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>