<div class="container margin-error">
    <div class="row">
        <div class="col-lg-10 mx-auto">
            <h1>Извините страница не найдена</h1>
            <p>Попробуйте исправить URL или перейти на главную страницу</p>
            <a href="/Index" class="page-link">Tasker - Главная</a>
        </div>
        <div class="col-lg-10 margin-top-history mx-auto text-center">
            <img src="/public/img/error.jpg" alt="RickRoll" class="img-fluid img-thumbnail img-error">
        </div>
    </div>
</div>