<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-secondary"><?= Html::encode($title) ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form class="" action="/admin/Users" method="post">
                <div class="form-group form-inline">
                    <label class="text-secondary h5 margin-right-15" for="login">Поиск:</label>
                    <input type="text" name="search_user" class="form-control">
                    <input type="submit" name="submit" name="submit" class="btn btn-success form-control" value="Найти">
                </div>
            </form>
        </div>
    </div>
</div>
<?php if(!empty($users)):?>
    <div class = "container admin_top">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">
                    <?php foreach ($users as $user):?>
                        <li class =" list-group-item clearfix">
                            <div class = "float-lg-left">
                                <p>
                                    <span><b>#</b> <?= $user['id']?></span>
                                    <span><b>Логин:</b> <?= Html::encode($user['login']) ?></span>
                                    <span><b>Имя:</b> <?= Html::encode($user['name'])?></span>
                                    <span><b class="<?= Html::encode($colors[$user['banned']]) ?>"><?=Html::encode($ban_stats[$user['banned']])?></b> <?= ($user['banned'] == 1)? 'до '.date('Y-m-d H:i',$user['ban_date']): ''?></span>
                                    <span><b>Email: </b><?= Html::encode($user['email']) ?></span>
                                </p>
                                <span><b>Дата регистрации: </b><?= Html::convertTime($user['reg_date']) ?></span>
                            </div>
                            <div class = "float-lg-right">
                                <a href="/admin/Index/<?= $user['id'];?>" class="btn btn-success">Задачи</a>
                                <a href="/admin/BanUser/<?= $user['id'];?>" class="btn btn-primary">Блок.</a>
                                <a href="/admin/DeleteUser/<?= $user['id'];?>" class="btn btn-danger">Удалить</a>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
<?php else:?>
    <div class="container margin-top-history">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="text-secondary">Пользователей удовлетворяющих вашему поисковому запросу не найдено</h1>
            </div>
        </div>
    </div>
<?php endif;?>