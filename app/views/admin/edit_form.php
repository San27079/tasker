<div class="container">
    <div class="container d-flex justify-content-center main_page">
        <form action="/admin/EditForm" method="post" class="my-form">
            <input type="hidden" name = "id" value="<?= Html::encode($task['id'])?>">
            <h5>Редактировать задачу #<?= Html::encode($task['id'])?> </h5>
            <div class="form-group">
                <label  for = "title">Заголовок *</label>
                <input class="form-control" name="title" id = "title" placeholder="Введите название задачи" value="<?= Html::encode($task['title'])?>">
            </div>
            <div class="form-group">
                <label for="status">Изменить статус</label>
                <select class="form-control" name="stat" id="status">
                    <option value="0" <?= ($task['stat'] == 0)? 'selected' : '';?>>Новое</option>
                    <option value="1" <?= ($task['stat'] == 1)? 'selected' : '';?>>В процессе</option>
                    <option value="2" <?= ($task['stat'] == 2)? 'selected' : '';?>>Завершено</option>
                </select>
            </div>
            <div class="form-group">
                <label class = " h5" for = "task-text">Редактировать текст задачи</label>
                <textarea class="form-control" name="task-text" rows="6"><?= Html::encode($task['task_text'])?>
                </textarea>
            </div>
            <input type="submit" name="submit"  value="Обновить" class="btn btn-success">
        </form>
    </div>
</div>