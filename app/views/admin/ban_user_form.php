<div class="container">
    <div class="container d-flex justify-content-center main_page">
        <form action="/admin/BanUser" method="post" class="my-form">
            <input type="hidden" name = "id" value="<?= $user['id'];?>">
            <h4>Блокировать пользователя # <?= $user['id'];?> </h4>
            <br>
            <div class="form-group">
                <label for="status" class="h5">Изменить статус блокировки</label>
                <select class="form-control" name="banned" id="ban_status">
                    <option value="0" <?= ($user['banned'] == 0)? 'selected' : '';?>>Блокировки нет</option>
                    <option value="1" <?= ($user['banned'] == 1)? 'selected' : '';?>>Временно заблокировать</option>
                    <option value="2" <?= ($user['banned'] == 2)? 'selected' : '';?>>Заблокировать навечно</option>
                </select>
            </div>
            <br>
            <div class="form-group" id="date_block">
                <label for="ban_date" class="h5">Срок блокировки</label>
                <input type="date" name="ban_date" id="date_block" value="<?= Html::convertTime($user['ban_date'], 'Y-m-d') ?>">
            </div>
            <input type="submit" name="submit"  value="Обновить" class="btn btn-success">
        </form>
    </div>
</div>