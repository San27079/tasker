<div class="container margin-top-history">
    <div class="row">
        <h1 class="margin-header-bottom">
            <?= Html::encode($title.' '.$question['user_name']) ?>
        </h1>
        <div class="col-lg-6 mx-auto">
            <h3>Вопрос:</h3>
            <ul class="list-group">
                <li class="list-group-item"><b>От кого: </b><?= Html::encode($question['mail_from'])?> </li>
                <li class="list-group-item"><b>Дата: </b><?= Html::convertTime($question['date_create'])?> </li>
                <li class="list-group-item"><b>Тема сообщения:</b><?= Html::encode($question['subject'])?> </li>
                <li class="list-group-item"><b>Сообщение: </b><?= Html::encode($question['text'])?> </li>
            </ul>
        </div>
        <div class="col-lg-6 mx-auto">
            <h3>Ответ:</h3>
            <ul class="list-group">
                <li class="list-group-item"><b>От кого: </b><?= Html::encode($answer['mail_from'])?> </li>
                <li class="list-group-item"><b>Дата: </b><?= Html::convertTime($answer['date_create'])?> </li>
                <li class="list-group-item"><b>Тема сообщения:</b><?= Html::encode($answer['subject'])?> </li>
                <li class="list-group-item"><b>Сообщение: </b><?= Html::encode($answer['text'])?> </li>
            </ul>
        </div>
    </div>
    <div class="row margin-top-history margin-header-bottom">
        <div class="col-lg-12">
            <a href="<?=Html::encode($back)?>">Назад</a>
        </div>
    </div>
</div>