<div class="container margin-top-history">
    <div class="row">
        <div class="col-lg-7 mx-auto">
            <h1 class="">
                <?= Html::encode($title) ?>
            </h1>
            <ul class="list-group">
                <li class="list-group-item"><b>От кого: </b><?= Html::encode($mail['mail_from'])?> </li>
                <li class="list-group-item"><b>Дата: </b><?= Html::convertTime($mail['date_create'])?> </li>
                <li class="list-group-item"><b>Тема сообщения:</b><?= Html::encode($mail['subject'])?> </li>
                <li class="list-group-item"><b>Сообщение: </b><?= Html::encode($mail['text'])?> </li>
            </ul>
            <h2 class="margin-top-history">Форма ответа на сообщение</h2>
            <?= (isset($error))? '<p class="bg-danger text-light">'.Html::encode($error).'<p>':''; ?>
            <form action="/admin/AnswerMail" method="post" class="form">
                <input type="hidden" value="<?=Html::encode($mail['id']);?>" name="parent_id">
                <div class="form-group">
                    <label class="" for="message">Сообщение</label>
                    <textarea name="message" id="text" class="form-control" placeholder="Введите текст сообщения" rows="6"></textarea>
                </div>
                <div class="g-recaptcha" data-sitekey="<?= Html::encode(Settings::getKey('g_site_key'))?>"></div>
                <input type="submit" id="submit" name="submit" value="Отправить" class="btn btn-success">

            </form>
        </div>
    </div>
</div>