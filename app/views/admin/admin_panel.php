<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-secondary"><?= Html::encode($title) ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form class="" action="/admin/Index" method="post">
                <div class="form-group form-inline">
                    <label class="text-secondary h5 margin-right-15" for="login">Поиск:</label>
                    <input type="text" name="search_word" class="form-control">
                    <input type="submit" name="submit" name="submit" class="btn btn-success form-control" value="Найти">
                </div>
            </form>
        </div>
    </div>
</div>
<?php if(!empty($tasks)):?>
<div class = "container admin_top">
    <div class="row">
        <div class="col-lg-12">
            <ul class="list-group">
                <?php foreach ($tasks as $task):?>
                    <li class =" list-group-item clearfix">
                        <div class = "float-lg-left">
                            <p>
                                <span><b># </b> <?= Html::encode($task['id'])?></span>
                                <span><b>Пользователь: </b> <?= Html::encode($task['user_id'])?></span>
                                <span><b>Статус: </b>  <span class="<?= Html::encode($colors[$task['stat']])?>"><?= Html::encode($statuses[$task['stat']])?></span></span>
                                <span><b>Заголовок: </b><?= Html::encode($task['title']) ?></span>
                                <span><b>Дата создания: </b><?= Html::encode(date( 'Y-m-d H:i', $task['date_create'])) ?></span>
                            </p>
                            <p><span><b>Текст: </b></span><?= Html::encode(mb_substr($task['task_text'], 0, 65).'...')?> </span></p>
                        </div>
                        <div class = "float-lg-right">
                            <a href="/admin/EditForm/<?= Html::encode($task['id']);?>" class="btn btn-primary">Изм.</a>
                            <a href="/admin/Delete/<?= Html::encode($task['id']);?>" class="btn btn-danger">Удалить</a>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>
<?php else:?>
    <div class="container margin-top-history">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="text-secondary"><?= Html::encode($error)?></h1>
            </div>
        </div>
    </div>
<?php endif;?>