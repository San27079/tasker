<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="text-secondary"><?= Html::encode($title) ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form class="" action="/admin/Mail" method="post">
                <div class="form-group form-inline">
                    <label class="text-secondary h5 margin-right-15" for="search_mail">Поиск:</label>
                    <input type="text" name="search_mail" class="form-control">
                    <input type="submit" name="submit" name="submit" class="btn btn-success form-control" value="Найти">
                </div>
            </form>
        </div>
    </div>
</div>
<?php if(!empty($mail)):?>
    <div class = "container admin_top">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-group">
                    <?php foreach ($mail as $ml):?>
                        <li class =" list-group-item clearfix">
                            <div class = "float-lg-left">
                                <p>
                                    <span><b>#</b> <?= $ml['id']?></span>
                                    <span><b>От кого:</b> <?= Html::encode($ml['mail_from']) ?></span>
                                    <span><b>Тема:</b> <?= Html::encode($ml['subject'])?></span>
                                    <span><b>Дата отправки: </b><?= Html::convertTime($ml['date_create']) ?></span>
                                </p>
                                <span><b>Текст: </b><?= Html::encode($ml['text']) ?></span>
                            </div>
                            <div class = "float-lg-right">
                                <?php if(!$ml['status']):?>
                                    <a href="/admin/AnswerMail/<?= $ml['id'];?>" class="btn btn-primary">Ответить</a>
                                <?php else:?>
                                    <a href="/admin/ShowDialog/<?= $ml['id'];?>" class="btn btn-success">Просмотреть</a>
                                <?php endif;?>
                                <a href="/admin/DeleteMail/<?= $ml['id'];?>" class="btn btn-danger">Удалить</a>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
<?php else:?>
    <div class="container margin-top-history">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="text-secondary">Почты не найдено</h1>
            </div>
        </div>
    </div>
<?php endif;?>