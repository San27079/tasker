<div class="container margin-top-history">
    <div class="row">
        <div class="col-lg-7 mx-auto">
            <h1 class="text-light">
                Обратная связь:
            </h1>
            <?= isset($success)? "<p class ='text-light bg-success'>".Html::encode($success)."</p>":'' ?>
            <?= isset($error)? "<p class ='text-light bg-danger'>".Html::encode($error)."</p>":'' ?>
            <form action="/feedback/Index" method="post" class="form">
                <div class="form-group">
                    <label class="text-light" for="email">Имя:</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Введите ваше имя">
                </div>
                <div class="form-group">
                    <label class="text-light" for="email">Email:</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Введите email">
                </div>
                <div class="form-group">
                    <label class="text-light" for="subject">Тема сообщения:</label>
                    <input type="text" name="subject" id="subject" class="form-control" placeholder="Введите тему сообщения">
                </div>
                <div class="form-group">
                    <label class="text-light" for="text">Сообщение</label>
                    <textarea name="message" id="text" class="form-control" placeholder="Введите текст сообщения" rows="6"></textarea>
                </div>
                <div class="g-recaptcha" data-sitekey="<?= Html::encode(Settings::getKey('g_site_key'))?>"></div>
                <input type="submit" id="submit" name="submit" value="Отправить" class="btn btn-success">

            </form>
            <p class="text-light">Перейти на <a href="/index/index">главную</a></p>
        </div>
    </div>
</div>