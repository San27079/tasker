<div class="container padding-top-reg">
    <div class="row">
        <div class="col-lg-6 ">
            <div class="border-rad-quote bg-light">
                <h2 class="">Tasker - Личный задачник</h2>
                <br>
                <p class="blockquote font-italic">
                    <i class="fas fa-quote-left"></i>
                    Этот небольшой сайт написан в помощь мне, чтобы упорядочить личные задачи. Если вам это тоже нужно, регистрируйтесь и пользуйтесь, это абсолютно бесплатно.
                    <i class="fas fa-quote-right"></i>
                </p>
            </div>
        </div>
        <div class="offset-lg-2 col-lg-4">
            <h1 class="text-light">
                Регистрация
            </h1>
            <?= isset($error)?"<p class = 'text-light bg-danger'>". Html::encode($error)."</p>" : ''; ?>
            <form action="/index/Registration" method="post" class="form">
                <div class="form-group">
                    <label class="text-light" for="login">Логин:*</label>
                    <input type="text" name="login" id="login" class="form-control" placeholder="Введите ваш логин">
                </div>
                <div class="form-group">
                    <label class="text-light" for="login">Имя:*</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Введите вашe имя">
                </div>
                <div class="form-group">
                    <label class="text-light" for="login">Email:*</label>
                    <input type="text" name="email" id="email" class="form-control" placeholder="Введите email">
                </div>
                <div class="form-group">
                    <label class="text-light" for="password">Пароль:*</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Введите пароль">
                </div>
                <div class="form-group">
                    <label class="text-light" for="password">Подтверждение пароля:*</label>
                    <input type="password" name="password-check" id="password-check" class="form-control" placeholder="Подтвердите пароль">
                </div>
                <div class="g-recaptcha" data-sitekey="<?= Settings::getKey('g_site_key'); ?>"></div>
                <input type="submit" id="submit" name="submit" value="Зарегистрироваться" class="btn btn-success">
            </form>
            <p class="text-light">Перейти на страницу <a href="/index/index">авторизации</a></p>
        </div>
    </div>
</div>