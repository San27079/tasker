<div class="container padding-top-login">
    <div class="row">
        <div class="col-lg-6 ">
            <div class="border-rad-quote bg-light">
                <h2 class="">Tasker - Личный задачник</h2>
                <br>
                <p class="blockquote font-italic">
                    <i class="fas fa-quote-left"></i>
                    Этот небольшой сайт написан в помощь мне, чтобы упорядочить личные задачи. Если вам это тоже нужно, регистрируйтесь и пользуйтесь, это абсолютно бесплатно.
                    <i class="fas fa-quote-right"></i>
                </p>
            </div>
        </div>
        <div class="offset-lg-2 col-lg-4">
            <h1 class="text-light">
                Войти
            </h1>
            <?= isset($success)? "<p class ='text-light bg-success'>".Html::encode($success)."</p>":'' ?>
            <?= isset($error)? "<p class ='text-light bg-danger'>".Html::encode($error)."</p>":'' ?>
            <form action="/index/Login" method="post" class="form">
                <div class="form-group">
                    <label class="text-light" for="login">Логин:</label>
                    <input type="text" name="login" id="admin-login" class="form-control">
                </div>
                <div class="form-group">
                    <label class="text-light" for="password">Пароль:</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
                <div class="g-recaptcha" data-sitekey="<?= Html::encode(Settings::getKey('g_site_key'))?>"></div>
                <input type="submit" id="submit" name="submit" value="Войти" class="btn btn-success">

            </form>
            <p class="text-light">Перейти на страницу <a href="/index/registration">регистрации</a></p>
        </div>
    </div>
</div>