<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 22.04.2018
 * Time: 21:53
 */

namespace app\models;

use app\core\Model;

class AuthorizationModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findUserByLogin($login, $admin = false){
        if(!$admin) {
            $sql = "SELECT * FROM users WHERE login=:login";
        }else{
            $sql = "SELECT * FROM tasker_admins WHERE login=:login";
        }
        $finded_login = $this->db->prepare($sql);
        $finded_login->execute(array(':login' => $login));
        $finded_login = $finded_login->fetch();
        return $finded_login;
    }

    public function registerNewUser($login, $password, $email, $name){
        $password_h = md5(md5(trim($password)));
        $sql = "INSERT INTO users (login, password, email, reg_date, name) VALUES (:login, :password, :email, :reg_date, :name)";
        $new_user = $this->db->prepare($sql);
        $new_user->execute(array(":login" => $login, ":password" => $password_h, ":email" => $email, ':reg_date' => time(), ':name' => $name));
        return true;
    }

    public function userAuth($id, $admin = false){
        $code = $this->generateCode();
        if(!$admin) {
            $sql = "UPDATE users SET hash=:code WHERE id=:id";
        }else{
            $sql = "UPDATE tasker_admins SET hash=:code WHERE id=:id";
        }
        $task = $this->db->prepare($sql);
        $task->execute(array(':code' => $code, ':id' => $id));
        return $code;
    }

    public function generateCode($length = 16){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
        $generated_code = '';
        $count_chars = strlen($chars) - 1;
        while (strlen($generated_code) < $length){
            $generated_code .= $chars[mt_rand(0, $count_chars)];
        }
        return $generated_code;
    }
}