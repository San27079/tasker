<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 16:04
 */

namespace app\models;

use app\core\Model;

class AdminModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllTasks()
    {
        $sql = "SELECT * FROM tasks";
        $tasks = $this->db->query($sql);
        $tasks = $tasks->fetchAll();
        return $tasks;
    }

    public function getFindedTasks($word)
    {
        $sql = "SELECT * FROM tasks WHERE title LIKE :word OR task_text LIKE :word";
        $tasks = $this->db->prepare($sql);
        $tasks->execute(array(':word' => "%$word%"));
        $tasks = $tasks->fetchAll();
        return $tasks;
    }

    public function getAllUsers()
    {
        $sql = "SELECT id, login, name, email, banned, ban_date, reg_date FROM users";
        $users = $this->db->query($sql);
        $users = $users->fetchAll();
        return $users;
    }

    public function getFindedUsers($user)
    {
        $sql = "SELECT id, login, name, email, banned, ban_date, reg_date FROM users WHERE name LIKE :word OR login LIKE :word";
        $users = $this->db->prepare($sql);
        $users->execute(array(':word' => "%$user%"));
        $users = $users->fetchAll();
        return $users;
    }

    public function deleteUser($id)
    {
        $sql = "DELETE FROM users WHERE id=:id";
        $user = $this->db->prepare($sql);
        if($user->execute(array(':id' => $id))){
            return true;
        }else{
            return false;
        }
    }

    public function getUserData($id)
    {
        $sql = "SELECT id, login, name, email, banned, ban_date, reg_date FROM users WHERE id = :id";
        $user = $this->db->prepare($sql);
        $user->execute(array(':id' => $id));
        $user = $user->fetch();
        return $user;
    }

    public function getUserTasks($id_user)
    {
        $sql = "SELECT id FROM tasks WHERE user_id = :id";
        $tasks = $this->db->prepare($sql);
        $tasks->execute(array(':id' => $id_user));
        $tasks = $tasks->fetchAll();
        return $tasks;
    }

    public function updateUserBanStatus($id, $date, $banned)
    {
        $sql = "UPDATE users SET banned = :banned, ban_date = :ban_date WHERE id=:id";
        $task = $this->db->prepare($sql);
        $task->execute(array(':id' => $id, ':ban_date' => $date, ':banned' => $banned));
        return true;
    }

    public function getAllUserTasks($id)
    {
        $sql = 'SELECT * FROM tasks WHERE user_id=:id';
        $tasks = $this->db->prepare($sql);
        $tasks->execute(array(':id' => $id));
        $tasks = $tasks->fetchAll();
        return $tasks;
    }

}