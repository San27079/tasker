<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 12:15
 */

namespace app\models;

use app\core\Model;

class IndexModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTasksPaginated($type, $offset, $id)
    {
        $sql = 'SELECT * FROM tasks WHERE user_id=:id AND history = 0 ORDER BY '.$type.' ASC LIMIT 3 OFFSET '.$offset;
        $tasks = $this->db->prepare($sql);
        $tasks->execute(array(':id' => $id));
        $tasks = $tasks->fetchAll();
        return $tasks;
    }

    public function getProgressTasksPaginated($offset, $id, $stat)
    {
        $sql = 'SELECT * FROM tasks WHERE user_id=:id AND history = 0 AND stat=:status ORDER BY date_create ASC LIMIT 3 OFFSET '.$offset;
        $tasks = $this->db->prepare($sql);
        $tasks->execute(array(':id' => $id, ':status' => $stat));
        $tasks = $tasks->fetchAll();
        return $tasks;
    }

    public function getCount($id)
    {
        $count = $this->db->prepare('SELECT count(*) FROM tasks WHERE history = 0 AND user_id = :id' );
        $count->execute(array(':id' => $id));
        $count = $count->fetch();
        return $count;
    }

    public function newTask($new_task)
    {
        $data[':user_id'] = $new_task['user_id'];
        $data[':text'] = $new_task['text'];
        $data[':date_create'] = $new_task['date'];
        $data['title'] = $new_task['title'];
        if(empty($new_task['image'])) {
            $sql = "INSERT INTO tasks ( user_id, date_create, task_text, title) VALUES (:user_id, :date_create, :text, :title)";
        }else{
            $data[':image'] = $new_task['image'];
            $sql = "INSERT INTO tasks ( user_id, date_create, task_text, image, title) VALUES (:user_id, :date_create, :text, :image, :title)";
        };
        $task = $this->db->prepare($sql);
        $task->execute($data);
        return true;
    }

    public function updateStatus($id, $stat)
    {
        switch ($stat){
            case 1:
                $sql = "UPDATE tasks SET stat =:status, change_date = :time_change WHERE id=:id";
                break;
            case 2:
                $sql = "UPDATE tasks SET stat =:status, change_date = :time_change, end_date = :time_change WHERE id=:id";
                break;
        }
        $task = $this->db->prepare($sql);
        $task->execute(array(':id' => $id, ':status' => $stat, ':time_change' => time()));
        return true;
    }

    public function moveToHistory($id)
    {
        $sql = "UPDATE tasks SET history = 1 WHERE id=:id";
        $task = $this->db->prepare($sql);
        $task->execute(array(':id' => $id));
        return true;
    }

    public function findHistory($word, $id)
    {
        $sql = "SELECT * FROM tasks WHERE user_id=:id AND history=1 AND title LIKE :word OR task_text LIKE :word";
        $task = $this->db->prepare($sql);
        $task->execute(array(':word' => '%'.$word.'%', ':id' => $id));
        $tasks = $task->fetchAll();
        return $tasks;
    }

}
