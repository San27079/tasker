<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 09.05.2018
 * Time: 21:50
 */

namespace app\models;

use app\core\Model;

class FeedbackModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setMail($subject, $text, $to, $from, $name, $parent_id = 0, $status = 0)
    {
        $data[':subject'] = $subject;
        $data[':text'] = $text;
        $data[':date_create'] = time();
        $data['parent_id'] = $parent_id;
        $data[':mail_from'] = $from;
        $data[':mail_to'] = $to;
        $data[':status'] = $status;
        $data[':user_name'] = $name;
        $sql = "INSERT INTO tasker_mail (subject, text, date_create, parent_id, mail_from, mail_to, status, user_name) VALUES (:subject, :text, :date_create, :parent_id, :mail_from, :mail_to, :status, :user_name)";
        $task = $this->db->prepare($sql);
        return $task->execute($data);
    }

    public function getMailDialog($id)
    {
        $sql = 'SELECT * FROM tasker_mail WHERE id = :id OR parent_id=:id';
        $mail = $this->db->prepare($sql);
        $mail->execute(array(':id' =>$id));
        $mail = $mail->fetchAll();
        return $mail;
    }

    public function getAllMail()
    {
        $sql = 'SELECT * FROM tasker_mail WHERE parent_id = 0';
        $mail = $this->db->prepare($sql);
        $mail->execute();
        $mail = $mail->fetchAll();
        return $mail;
    }

    public function getOneMail($id)
    {
        $sql = 'SELECT * FROM tasker_mail WHERE id = :id';
        $mail = $this->db->prepare($sql);
        $mail->execute(array(':id' =>$id));
        $mail = $mail->fetch();
        return $mail;
    }

    public function findMailDialog($request)
    {
        $sql = "SELECT * FROM tasker_mail WHERE parent_id = 0 AND subject LIKE :word OR text LIKE :word";
        $mail = $this->db->prepare($sql);
        $mail->execute(array(':word' => '%'.$request.'%'));
        $mail = $mail->fetchAll();
        return $mail;
    }

    public function deleteMail($id)
    {
        $sql = "DELETE FROM tasker_mail WHERE id=:id OR parent_id = :id";
        $mail = $this->db->prepare($sql);
        return $mail->execute(array(':id' => $id));
    }

    public function updateMailStatus($parent_id)
    {
        $sql = "UPDATE tasker_mail SET status = 1 WHERE id=:parent_id";
        $update_mail = $this->db->prepare($sql);
        return $update_mail->execute(array(':parent_id' => $parent_id));
    }
}