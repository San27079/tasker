<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 03.05.2018
 * Time: 23:56
 */

namespace app\models;

use app\core\Model;

class NotesModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getNote($id)
    {
        $sql = 'SELECT * FROM tasker_notes WHERE id_task=:id';
        $notes = $this->db->prepare($sql);
        $notes->execute(array(':id' => $id));
        $notes = $notes->fetchAll();
        return $notes;
    }

    public function newNote($text, $id_task)
    {
        $sql = "INSERT INTO tasker_notes (id_task, date_create, text_note) VALUES (:id_task, :date_create, :text)";
        $task = $this->db->prepare($sql);
        $final = $task->execute(array(':id_task' => $id_task, ':date_create' => time(), ':text' => $text));
        return $final;
    }

    public function delNote($id)
    {
        $sql = "DELETE FROM tasker_notes WHERE id=:id";
        $task = $this->db->prepare($sql);
        $final = $task->execute(array(':id' => $id));
        return $final;
    }
}
