<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 09.05.2018
 * Time: 18:43
 */
namespace app\controllers;

use app\core\Controller;
use app\helpers\Email;

class Feedback extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function actionIndex()
    {
        $data['title'] = 'Tasker - Обратная связь';
        $data['scripts'][] = 'feedback.js';
        $data['error'] = '';
        if($_SERVER['REQUEST_METHOD'] == 'POST' && $this->model->checkReCaptha($_POST['g-recaptcha-response'])){
            if(empty(trim($_POST['message'])) || empty(trim($_POST['email'])) || empty(trim($_POST['subject'])) && empty(trim($_POST['name']))){
                $data['error'] .= 'Все поля должны быть заполнены';
                $this->view->viewRender('feedback/no-auth-feedback', 'layouts/feedback-header', true, $data);
            }
            if(!preg_match("/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z]{1,4}$/", $_POST['email'])){
                $data['error'] .= "Введите корректно email";
                $this->view->viewRender('feedback/no-auth-feedback', 'layouts/feedback-header', true, $data);
            }

            $email = trim($_POST['email']);
            $subject = htmlspecialchars(trim($_POST['subject']));
            $text = htmlspecialchars(trim($_POST['message']));
            $name = htmlspecialchars(trim($_POST['name']));

            if(Email::sendMailAdmin($subject, $text, $this->storage->getData('email_admin'),$email, $this->storage->getData('domain').'/admin') && $this->model->setMail($subject, $text, $this->storage->getData('email_admin'), $email, $name)){
                $data['success'] .= 'Ваше сообщение успешно отправлено, постараюсь ответить в кратчайшие сроки';
                $this->view->viewRender('feedback/no-auth-feedback', 'layouts/feedback-header', true, $data);
            }else{
                $data['error'] .= 'Что пошло не так, попробуйте немного позже';
                $this->view->viewRender('feedback/no-auth-feedback', 'layouts/feedback-header', true, $data);
            }

        }else{
            $this->view->viewRender('feedback/no-auth-feedback', 'layouts/feedback-header', true, $data);
        }
    }
}
