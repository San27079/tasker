<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 03.05.2018
 * Time: 23:53
 */

namespace app\controllers;

use app\core\Controller;

class Notes extends Controller
{
    public function __construct()
    {
        parent:: __construct();
    }

    public function actionGet(){
        $this->checkAuthUser();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['note'])){
                $notes = $this->model->getNote($_POST['note']);
                echo json_encode($notes);
                exit();
            }
        }
    }

    public function actionAppendNote()
    {
        $this->checkAuthUser();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['text']) && isset($_POST['id'])){
                if($this->model->newNote($_POST['text'], $_POST['id'])){
                    echo 'true';
                }else{
                    echo 'false';
                }
            }else{
                echo 'false';
            }
        }
    }
    public function actionDeleteNote()
    {
        $this->checkAuthUser();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['id'])){
                if($this->model->delNote($_POST['id'])){
                    echo 'true';
                }else{
                    echo 'false';
                }
            }else{
                echo 'false';
            }
        }
    }
}