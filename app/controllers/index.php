<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 10:53
 */

namespace app\controllers;

use app\core\Controller;

class Index extends Controller
{
    public function __construct()
    {
        parent:: __construct();
    }

    public function actionIndex($parameters)
    {
        $this->checkAuthUser();
        $data['scripts'][] = 'notes.js';
        $parameters = $this->filterParameters($parameters);
        $c_params = count($parameters);
        $data['active_page'] = $parameters[1];
        $parameters[1] = $parameters[1] * 3;
        $_SESSION['sort_type'] = $parameters[0];
        $count_pages = $this->model->getCount($_SESSION['id'])[0] / 3;
        $count_pages = ceil($count_pages);
        if($c_params === 3){
            $tasks = $this->model->getProgressTasksPaginated($parameters[1], $_SESSION['id'], $parameters[2]);
        }else{
            $tasks = $this->model->getTasksPaginated($parameters[0], $parameters[1], $_SESSION['id']);
        }
        $data['title'] = 'Tasker - Мои задачи';
        $data['tasks'] = $tasks;
        $data['count'] = $count_pages;
        $this->view->viewRender('index/main_page', true, true, $data);
    }

    public function actionChangeStatus($parameters)
    {
        $this->checkAuthUser();
        if(!empty($parameters[0]) && !empty($parameters[1])){
            $this->model->updateStatus($parameters[0], $parameters[1]);
        }
        header('Location: '.$_SERVER['HTTP_REFERER'], true);
    }

    public function actionDelete($parameters)
    {
        $this->checkAuthUser();
        if(!empty($parameters[0])){
            $this->model->deleteTask($parameters[0]);
        }
        header('Location: '.$_SERVER['HTTP_REFERER'], true);
    }

    public function actionChange($parameters)
    {
        if(isset($_POST['submit'])) {
            if(!empty($_POST['id']) && !empty($_POST['task-text']) && !empty($_POST['title'])) {
                $this->model->updateTask($_POST['id'], $_POST['task-text'], $_POST['stat'], $_POST['title']);
                header('Location: '.$this->storage->getData('domain').'/', true);
            }
        }
        $task = $this->model->getTask($parameters[0]);
        $data['title'] = 'Edit task #'.$parameters[0];
        $data['task'] = $task;
        $this->view->viewRender('index/edit_form', true, true, $data);
    }

    public function actionHistory()
    {
        $this->checkAuthUser();
        $data['title'] = 'Tasker - История задач';
        $data['scripts'][] = 'history.js';
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['submit']) && isset($_POST['word'])) {
                $data['tasks'] = $this->model->findHistory($_POST['word'], $_SESSION['id']);
            }
        }else{
            $data['tasks'] = $this->model->getAllHistoryTasks($_SESSION['id']);
        }
        $this->view->viewRender('index/history_list', true, true, $data);
    }

    public function actionToHistory($parameters){
        $this->checkAuthUser();
        if(!empty($parameters[0])){
            $this->model->moveToHistory($parameters[0]);
        }
        header('Location: '.$_SERVER['HTTP_REFERER'], true);
    }

    private function filterParameters($parameters)
    {
        if(empty($parameters[0]) && empty($parameters[1])){
            $parameters[0] = 'date_create';
            $parameters[1] = '0';
        }else if(empty($parameters[1])){
            $parameters[1] = 0;
        };
        switch ($parameters[0]) {
            case 'stat':
                $parameters[0] = "stat";
                break;
            case 'date_create':
                $parameters[0] = "date_create";
                break;
            case 'stat_active':
                $parameters[0] = "stat";
                $parameters[2] = 1;
                break;
        };
        return $parameters;
    }

    public function actionCreate()
    {
        $this->checkAuthUser();
        $data['title'] = 'Tasker - New Task';
        $data['scripts'][0] = 'create-script.js';
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(empty($_SESSION['id']) || empty($_POST['task-text']) || empty($_POST['title'])){
                $data['errors'] = 'Пожалуйста введите корректные данные';
                $this->view->viewRender('index/create_form', true, true, $data);
                exit;
            }else{
                $new_task['image'] = '';
                if(!empty($_FILES['image']['name'])) {
                    $upload_dir = 'img/tasks/';
                    $time = time();
                    $upload_path = $upload_dir.($_SESSION['id']*17925).'-'.$time.'-'.$_FILES['image']['name'];
                    copy($_FILES['image']['tmp_name'], $upload_path);
                    $this->createImage($upload_path, 640);
                    $new_task['image'] = '/'.$upload_path;
                }
                $new_task['user_id'] = $_SESSION['id'];
                $new_task['text'] = $_POST['task-text'];
                $new_task['title'] = $_POST['title'];
                $new_task['date'] = time();
                $this->model->newTask($new_task);
                header('Location: '.$this->storage->getData('domain').'/', true);
            }
        }else{
            $this->view->viewRender('index/create_form', true, true, $data);
        }
    }

    public function actionHistoryTaskReturn()
    {
        $this->checkAuthUser();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['id']) && is_int((int)$_POST['id'])){
                $this->loadOtherModel('notes');
                $notes = $this->notes->getNote($_POST['id']);
                $task = $this->model->getTask($_POST['id']);
                echo json_encode(['task' => $task, 'notes' => $notes]);
                exit();
            }
        }
    }

    public function actionRegistration()
    {
        $data['title'] = 'Tasker - Страница регистрации';
        if (isset($_POST['submit'])) {
            //return error because !require data
            if(empty($_POST['login']) || empty($_POST['email']) || empty($_POST['password']) || empty($_POST['password-check'])){
                $data['error'] .= "Заполните все поля.";
                $this->view->viewRender('authorization/registration', 'layouts/auth_header', true, $data);
            }
            if(!preg_match("/^[a-zA-Z0-9]+$/", $_POST['login']) || (strlen($_POST['login']) < 3) || (strlen($_POST['login']) > 30)){
                $data['error'] .= "Логин должен состоять только из букв и цифр. И иметь длину 3 - 30 символов";
            }

            if(!preg_match("/^[a-z0-9а-яё]+$/iu", $_POST['name']) || (strlen($_POST['name']) < 2) || (strlen($_POST['name']) > 30)){
                $data['error'] .= "Имя должно состоять только из букв и цифр. И иметь длину 2 - 30 символов";
            }

            if(!preg_match("/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z]{1,4}$/", $_POST['email'])){
                $data['error'] .= "Введите корректно email";
            }
            if((strlen($_POST['password']) < 7) || strcmp($_POST['password'], $_POST['password-check']) !== 0){
                $data['error'] .= "Ваш пароль короче 8 символов или пароли не совпадают";
            }
            $this->loadOtherModel('authorization');
            if(!isset($data['error']) && $this->authorization->checkReCaptha($_POST['g-recaptcha-response'])){
                if(count($this->authorization->findUserByLogin($_POST['login'])) === 1){
                    if($this->authorization->registerNewUser($_POST['login'], $_POST['password'], $_POST['email'], $_POST['name'])){
                        $data['success'] .= 'Регистрация прошла успешно можете авторизоваться';
                        $this->view->viewRender('authorization/login', 'layouts/auth_header', true, $data);
                    }
                }else{
                    $data['error'] .= 'Пользователь с таким именем уже зарегистрирован в системе';
                    $this->view->viewRender('authorization/registration', 'layouts/auth_header', true, $data);
                }
            }else{
                $this->view->viewRender('authorization/registration', 'layouts/auth_header', true, $data);
            }
        } else {
            $this->view->viewRender('authorization/registration', 'layouts/auth_header', true, $data);
        }
    }

    public function actionLogin()
    {
        $data['title'] = 'Tasker - Страница входа';
        $this->loadOtherModel('authorization');
        if(isset($_POST['submit'])){
            if(empty($_POST['login']) || empty($_POST['password'])){
                $data['error'] .= 'Все поля должны быть заполнены';
                $this->view->viewRender('authorization/login', 'layouts/auth_header', true, $data);
                exit();
            }
            if(count($user = $this->authorization->findUserByLogin($_POST['login'])) !== 1 && $this->authorization->checkReCaptha($_POST['g-recaptcha-response'])){
                if($user['banned'] == 1 && $user['ban_date'] > time()){
                    $data['error'] .= "Вы забанены на срок до ". date('Y-m-d H:i', $user['ban_date']);
                    $this->view->viewRender('authorization/login', 'layouts/auth_header', true, $data);
                    exit();
                }else if($user['banned'] == 2){
                    $data['error'] .= 'Вы забанены на вечно, к сожалению доступ к сайту для вас заблокирован';
                    $this->view->viewRender('authorization/login', 'layouts/auth_header', true, $data);
                    exit();
                }else {
                    $password = md5(md5(trim($_POST['password'])));
                    if ($user['password'] === $password) {
                        $code = $this->authorization->userAuth($user['id']);
                        $_SESSION['id'] = $user['id'];
                        $_SESSION['hash'] = $code;
                        $_SESSION['name'] = $user['name'];
                        header("Location: ".$this->storage->getData('domain') ."/index/index", true);
                    } else {
                        $data['error'] .= 'Проверьте правильность ввода логина и пароля';
                        $this->view->viewRender('authorization/login', 'layouts/auth_header', true, $data);
                        exit();
                    }
                }
            }else{
                $data['error'] .= 'Проверьте правильность ввода логина и пароля';
                $this->view->viewRender('authorization/login', 'layouts/auth_header', true, $data);
                exit();
            }
        }
        $this->view->viewRender('authorization/login', 'layouts/auth_header', true, $data);
    }

    public function actionLogout(){
        $this->destroySessionData();
        header('Location: '.$this->storage->getData('domain').'/');
    }
}