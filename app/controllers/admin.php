<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 12:30
 */

namespace app\controllers;

use app\core\Controller;
use app\helpers\Email;

class Admin extends Controller
{
    public function __construct()
    {
        parent:: __construct();
    }

    //Tasks show functions
    public function actionIndex($params = '')
    {
        $this->checkAuthAdmin();
        $data['title'] = 'Tasker_admin - Все события';
        $data['colors'] = $this->storage->getData('colors_stats');
        $data['statuses'] = $this->storage->getData('statuses');
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['submit']) && !empty($_POST['search_word'])){
                $data['tasks'] = $this->model->getFindedTasks($_POST['search_word']);
            }else{
                $data['tasks'] = $this->model->getAllTasks();
            }
        }else if(!empty($params[0])){
            $data['title'] = 'Tasker_admin - Все события пользователя #'.$params[0];
            $data['tasks'] = $this->model->getAllUserTasks($params[0]);
        }else{
            $data['tasks'] = $this->model->getAllTasks();
        }
        $this->view->viewRender('admin/admin_panel', 'layouts/admin_header', true, $data);
    }
    //Tasks edit functions
    public function actionEditForm($id){
        $this->checkAuthAdmin();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['submit'])) {
                if(!empty($_POST['id']) && !empty($_POST['task-text']) && !empty($_POST['title'])) {
                    $this->model->updateTask($_POST['id'], $_POST['task-text'], $_POST['stat'], $_POST['title']);
                    header('Location: '.$this->storage->getData('domain') .'/admin/Index', true);
                }
            }
        }else{
            $task = $this->model->getTask($id[0]);
            $data['title'] = 'Edit task #'.$id[0];
            $data['task'] = $task;
            $this->view->viewRender('admin/edit_form', true, true, $data);
        }
    }

    public function actionDelete($id)
    {
        $this->checkAuthAdmin();
        $this->model->deleteTask($id[0]);
        header('Location: '.$this->storage->getData('domain') .'/admin', true);
    }

    //Users functions
    public function actionUsers()
    {
        $this->checkAuthAdmin();
        $data['title'] = 'Tasker_admin - Пользователи';
        $data['ban_stats'] = $this->storage->getData('ban_stats');
        $data['colors'] = $this->storage->getData('colors_ban');
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['submit']) && !empty($_POST['search_user'])){
                $data['users'] = $this->model->getFindedUsers($_POST['search_user']);
            }else{
                $data['users'] = $this->model->getAllUsers();
            }
        }else{
            $data['users'] = $this->model->getAllUsers();
        }
        $this->view->viewRender('admin/users_panel', 'layouts/admin_header', true, $data);
    }

    public function actionBanUser($params)
    {
        $this->checkAuthAdmin();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['submit']) && isset($_POST['banned']) && isset($_POST['id'])){
                $time_array = explode('-', $_POST['ban_date']);
                $date = mktime(0,0,0, $time_array[1], $time_array[2], $time_array[0]);
                $this->model->updateUserBanStatus($_POST['id'], $date, $_POST['banned']);
            }else{
                header('Location: '.$_SERVER['HTTP_REFERER'] , true);
            }
            $this->actionUsers();
        }else{
            $data['title'] = 'Блокировка пользователя';
            $data['user'] = $this->model->getUserData($params[0]);
            $data['scripts'][] = 'ban_state.js';
            $this->view->viewRender('admin/ban_user_form', 'layouts/admin_header', true, $data);
        }
    }

    public function actionDeleteUser($params)
    {
        $this->checkAuthAdmin();
        $id = $params[0];
        if(!empty($id)){
            $tasks = $this->model->getUserTasks($id);
            foreach ($tasks as $task){
                $this->model->deleteTask($task['id']);
            }
            $this->model->deleteUser($id);
        }
        header('Location: '.$this->storage->getData('domain') .'/admin/Users', true);
    }
    /*
     * Mail functions
     *
     */
    public function actionMail()
    {
        $this->checkAuthAdmin();
        $this->loadOtherModel('feedback');
        $data['title'] = 'Tasker - Почта';
        if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['search_mail'])){
            $word = trim($_POST['search_mail']);
            $data['mail'] = $this->feedback->findMailDialog($word);
            $this->view->viewRender('admin/mail_list', 'layouts/admin_header', true, $data);
        }else {
            $data['mail'] = $this->feedback->getAllMail();
            $this->view->viewRender('admin/mail_list', 'layouts/admin_header', true, $data);
        }
    }

    public function actionDeleteMail($params)
    {
        $this->checkAuthAdmin();
        $this->loadOtherModel('feedback');
        if(empty(trim($params[0]))){
            header('Location: '.$this->storage->getData('domain') .'/admin/Mail', true);
        }
        $id = trim($params[0]);
        if($this->feedback->deleteMail($id)){
            header('Location: '.$this->storage->getData('domain') .'/admin/Mail', true);
        }

    }

    public function actionAnswerMail($params ='')
    {
        $this->checkAuthAdmin();
        $data['title'] = 'Tasker- Ответ на почту';
        $this->loadOtherModel('feedback');
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['submit']) && !empty(trim($_POST['parent_id'])) && !empty(trim($_POST['message']))){
                $text = htmlspecialchars(trim($_POST['message']));
                $parent_id = trim($_POST['parent_id']);
                $mail = $this->feedback->getOneMail($parent_id);
                if($mail['status']){
                    $data['error'] = 'Ответ на сообщение уже существует.';
                    $this->view->viewRender('admin/answer_mail', 'layouts/admin_header', true, $data);
                }
                $base_mail = $this->feedback->setMail($mail['subject'], $text, $mail['mail_from'], $this->storage->getData('email_admin'), $mail['user_name'], $mail['id']);
                $status_update = $this->feedback->updateMailStatus($parent_id);
                $send_mail = Email::sendMailUser($mail['subject'], $text, $mail['mail_from'], $this->storage->getData('email_admin'), $this->storage->getData('domain'), $mail['user_name']);
                if(!$base_mail || !$send_mail || !$status_update){
                    $data['error'] = 'Что то пошло не так попробуйте еще раз попозже.';
                    $this->view->viewRender('admin/answer_mail', 'layouts/admin_header', true, $data);
                }else{
                    header('Location: '.$this->storage->getData('domain') .'/admin/Mail', true);
                }
            }else{
                $data['error'] = 'Заполните все данные';
                $this->view->viewRender('admin/answer_mail', 'layouts/admin_header', true, $data);
            }
        }else{
            if(empty(trim($params[0]))){
                header('Location: '.$this->storage->getData('domain') .'/admin/Mail', true);
            }
            $id = trim($params[0]);
            $data['mail'] = $this->feedback->getOneMail($id);
            $this->view->viewRender('admin/answer_mail', 'layouts/admin_header', true, $data);
        }
    }

    public function actionShowDialog($params)
    {
        $this->checkAuthAdmin();
        $this->loadOtherModel('feedback');
        $data['title'] = 'Tasker - Переписка с пользователем';
        if(!empty(trim($params[0]))){
            $id = trim($params[0]);
            $dialog = $this->feedback->getMailDialog($id);
            if(empty($dialog)){
                header('Location: '.$this->storage->getData('domain') .'/admin/Mail', true);
            }
            $data['question'] = $dialog[0];
            $data['answer'] = $dialog[1];
            $data['back'] = $_SERVER['HTTP_REFERER'];
            $this->view->viewRender('admin/one_dialog', 'layouts/admin_header', true, $data);
        }else{
            header('Location: '.$this->storage->getData('domain') .'/admin/Mail', true);
        }
    }
    //===================================================================================================================
    //AuthAdmin

    public function actionLogin()
    {
        $data['scripts'][0] = 'login.js';
        $data['title'] = 'Tasker - Панель администратора';
        $this->loadOtherModel('authorization');
        if (isset($_POST['submit'])) {
            if (empty($_POST['login']) || empty($_POST['password'])) {
                $data['error'] .= 'Все поля должны быть заполнены';
                $this->view->viewRender('admin/login_admin', 'layouts/auth_header', true, $data);
                exit();
            }
            if (count($user = $this->authorization->findUserByLogin($_POST['login'], true)) !== 1 && $this->authorization->checkReCaptha($_POST['g-recaptcha-response'])) {
                $password = md5(md5(trim($_POST['password'])));
                if ($user['password'] === $password) {
                    $code = $this->authorization->userAuth($user['id'], true);
                    $_SESSION['id_admin'] = $user['id'];
                    $_SESSION['hash_admin'] = $code;
                    $_SESSION['name_admin'] = $user['name'];
                    header("Location: ".$this->storage->getData('domain') ."/admin/index", true);
                } else {
                    $data['error'] .= 'Проверьте правильность ввода логина и пароля';
                    $this->view->viewRender('admin/login_admin', 'layouts/auth_header', true, $data);
                    exit();
                }
            } else {
                $data['error'] .= 'Проверьте правильность ввода логина и пароля';
                $this->view->viewRender('admin/login_admin', 'layouts/auth_header', true, $data);
                exit();
            }
        }
        $this->view->viewRender('admin/login_admin', 'layouts/auth_header', true, $data);
    }

    public function actionLogout(){
        $this->destroySessionDataAdmin();
        header('Location: '.$this->storage->getData('domain') .'/', true);
    }
}