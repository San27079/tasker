var NotesLinks = $('.notes-links');
var NoteBody = $('#notes-body');
var GlobalId;
var NotesTitle = $('#notesModalLabel');
var OpenForm = $('#open-form');
var FormNote = $('#form-note');
var Close = $('.close');
var CloseButton = $('.button-close');
//Main Stack
$(document).ready(function () {
    FormNote.hide();
    NotesLinks.click(
       function(event){
           event.preventDefault(false);
           getNote($(this).data('note'), false);
       }
    );
    OpenForm.click(function(){
        $('#error-note').text('');
        appendNoteForm();
        ValObj.CheckButton();
        $('#text-note').change(ValObj.Text);
        $('#submit-note').click(sendNote);
    });
    Close.click(function(){
        closeNoteForm();
        $('#error-note').text('');
    });
    CloseButton.click(function(){
        closeNoteForm();
        $('#error-note').text('');
    });
});
//-----------------------------------------------------------------
function getNote(id, afterNew){
    GlobalId = id;
    $.ajax
    ({
        method: 'POST',
        url: '/notes/Get',
        data: {'note': id},
        dataType: 'json',
        complete: function (text) {
            data = text.responseText;
            data = JSON.parse(data);
            if(!afterNew){
                if(data){
                    openWindow(data);
                }else{
                    openWindow(false);
                }
            }else{
                appendListData(data);
            }
            $('.delete-note').click(
                function(){
                    deleteNote($(this).data('note-del'));
                }
            );
        }
    });
}

function openWindow(data)
{
    if(data != false){
        appendListData(data);
        $('#notesModal').modal();
    }else{
        appendEmptyData();
        $('#notesModal').modal();
    }
}
function appendListData(data)
{
    var info = '<ul class="list-group">';
    for(i = 0; i < data.length; i++){
        info += '<li class="list-group-item">' +
                    '<b># </b>'+ data[i].id +
                    '<b> Дата: </b>'+ convertDate(data[i].date_create) +
                    '<b> Заметка: </b>'+ data[i].text_note +
                    '<button class="btn btn-danger  margin-left-15 delete-note float-right" data-note-del='+ data[i].id +'><i class="fas fa-trash-alt fa-1x"></i></button>'+
                '</li>';
    }
    info += '</ul>';
    NotesTitle.text('Заметки для задачи #'+ GlobalId);
    NoteBody.html(info);
}

function appendEmptyData()
{
    var info = '<h5>Заметок задачи #'+ GlobalId +' пока нет, но вы можете их добавить</h5>';
    NotesTitle.text('Заметки для задачи #'+ GlobalId);
    NoteBody.html(info);
}

function convertDate(unix)
{
    var NewDate = new Date(unix*1000);
    var months = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Июнь','Сентябрь','Октябрь','Ноябрь','Декабрь'];
    var year = NewDate.getFullYear();
    var month = months[NewDate.getMonth()];
    var date = NewDate.getDate();
    var hour = NewDate.getHours();
    var min = NewDate.getMinutes();
    var sec = NewDate.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
}

function appendNoteForm()
{
    var form = '<div class="form">' +
                    '<div class="form-group">' +
                        '<label  for = "title">Текст заметки *</label>' +
                        '<textarea class="form-control" name="text-note" id = "text-note" placeholder="Введите текст заметки"></textarea>' +
                        '<button id="submit-note" class="btn btn-primary margin-top-history">Добавить</button>'+
                        '<button class=" margin-left-15 btn btn-secondary margin-top-history close-form">Скрыть форму</button>'+
                    '</div>'+
                '</div>';
    FormNote.html(form);
    FormNote.show('slow');
    $('.close-form').click(closeNoteForm);
}

function closeNoteForm() {
    $('#textNote').text('');
    FormNote.hide('slow');
}

ValObj = {
    'Status' : {text_stat : 0, pass_stat : 0},
    'Text' : function(){
        var TextNote = $('#text-note');
        if(TextNote.val().length >= 3){
            TextNote.removeClass('is-invalid');
            TextNote.addClass('is-valid');
            ValObj.Status.text_stat = 1;
            ValObj.CheckButton();
        }else{
            TextNote.removeClass('is-valid');
            TextNote.addClass('is-invalid');
        }
    },
    'CheckButton' : function(){
        if(ValObj.Status.text_stat){
            $('#submit-note').prop('disabled', false);
        }else{
            $('#submit-note').prop('disabled', true);
        }
    }
};

function sendNote(){
    ValObj.Status.text_stat = 0;
    var SendText = $('#text-note').val();
    $.ajax
    ({
        method: 'POST',
        url: '/notes/AppendNote',
        data: {'text': SendText, 'id': GlobalId},
        dataType: 'text',
        complete: function (text) {
            answer = text.responseText;
            if(answer === 'true'){
                closeNoteForm();
                $('#error-note').html('<span class="text-success">Ваша заметка успешно добавлена<span>');
                getNote(GlobalId, true);
            }else{
                closeNoteForm();
                $('#error-note').html('<span class="text-danger">Произошла ошибка, попробуйте немного позже</span>');
            }
        }
    });
}

function deleteNote(id)
{
    $.ajax
    ({
        method: 'POST',
        url: '/notes/DeleteNote',
        data: {'id': id},
        dataType: 'text',
        complete: function (text) {
            answer = text.responseText;
            if(answer === 'true'){
                closeNoteForm();
                $('#error-note').html('<span class="text-success">Ваша заметка успешно удалена<span>');
                getNote(GlobalId, true);
            }else{
                closeNoteForm();
                $('#error-note').html('<span class="text-danger">Произошла ошибка, попробуйте немного позже</span>');
            }
        }
    });
}