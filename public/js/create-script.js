//Init vars
var Title = $('#title');
var EmField = $('#email');
var Text = $('#text');
var Image = $('#image');
var BtnPrev = $('#preview-button');
var BtnHide = $('#hide-button');
var TitlePrev = $('#preview-title');
var EmailPrev = $('#preview-email');
var ImagePrev = $('#preview-img');
var TextPrev = $('#preview-text');
var CardPrev = $('#preview-card');
var DateCreate = $('#create_date');
//Validation admin input
ValObj = {
    'Status' : {title_stat : 0, email_stat : 0, text_stat : 0, file_stat : 0},
    'Title' : function(){
        if(Title.val().length >= 3){
            Title.removeClass('is-invalid');
            Title.addClass('is-valid');
            ValObj.Status.title_stat = 1;
            ValObj.CheckButton();
        }else{
            Title.removeClass('is-valid');
            Title.addClass('is-invalid');
        }
    },
    'Email' : function(){
        if(/^[0-9a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}$/.test(EmField.val())){
            EmField.removeClass('is-invalid');
            EmField.addClass('is-valid');
            ValObj.Status.email_stat = 1;
            ValObj.CheckButton();
        }else{
            EmField.removeClass('is-invalid');
            EmField.addClass('is-invalid');
        }
    },
    'FileFormat' : function(){
        if(/^.+\.jpg|gif|png$/i.test(Image.val()) || !Image.val()){
            Image.removeClass('is-invalid');
            Image.addClass('is-valid');
            ValObj.Status.image_stat = 1;
            ValObj.CheckButton();
        }else{
            Image.removeClass('is-valid');
            Image.addClass('is-invalid');
        }
    },
    'Text' : function(){
        if(Text.val().length >= 5 && Text.val().length <= 600){
            Text.removeClass('is-invalid');
            Text.addClass('is-valid');
            ValObj.Status.text_stat = 1;
            ValObj.CheckButton();
        }else{
            Text.removeClass('is-valid');
            Text.addClass('is-invalid');
        }
    },
    'CheckButton' : function(){
        if(ValObj.Status.text_stat && ValObj.Status.title_stat){
            $('#submit').prop('disabled', false);
        }else{
            $('#submit').prop('disabled', true);
        }
    }
};

$(document).ready(function(){
    ValObj.FileFormat();
    Text.val("");
    Title.val("");
    Image.val("");
    ValObj.CheckButton();
    Title.change(ValObj.Title);
    Text.change(ValObj.Text);
    Image.change(ValObj.FileFormat);
    BtnPrev.click(Preview);
    Image.change(function(event){SrcImg = URL.createObjectURL(event.target.files[0])})
});
//Preview
function Preview(){
    if(BtnPrev.data('status') === 1) {
        TitlePrev.text(Title.val());
        var list = '';
        var text = Text.val().split('\n');
        for(i = 0; i < text.length; i++){
            list += '<li class = "list-group-item">'+text[i]+'</li>';
        };
        TextPrev.html(list);
        if (!Image.val()) {
            SrcImg = '/public/img/tasks/default.jpg';
        }
        var date = new Date;
        DateCreate.html('Дата создания: ' + date.getFullYear()+'-'+(1+date.getMonth())+'-'+(1+date.getDay())+' '+date.getHours()+':'+date.getMinutes());
        CardPrev.css('background-image','URL('+SrcImg+')');
        BtnPrev.data('status', 2);
        BtnPrev.html("Скрыть просмотр");
        CardPrev.show('slow');
    }else if(BtnPrev.data('status') === 2){
        BtnPrev.data('status', 1);
        BtnPrev.html("Предпросмотр");
        CardPrev.hide('slow');
    }
}
