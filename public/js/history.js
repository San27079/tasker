var HistoryClassOver = $('.history-task-overview');
var GlobalId;
var PrevTitle = $('#preview-title');
var PrevText = $('#preview-text');
var HistoryCard = $('#history-card-prev');
var CreateDate = $('#create_date');
var NotesList = $('#notes-list');
var DeleteItem = $('#delete-item');
$(document).ready(function () {
    HistoryClassOver.click(
        function (event) {
            event.preventDefault(false);
            clearData();
            getTask($(this).data('id-task'));
            openWindow();
        }
    );

});
function openWindow()
{
    $('#historyModal').modal();
}

function getTask(id)
{
    GlobalId = id;
    $.ajax
    ({
        method: 'POST',
        url: '/index/HistoryTaskReturn',
        data: {'id': id},
        dataType: 'json',
        complete: function (text) {
            data = text.responseText;
            data = JSON.parse(data);
            appendData(data);
        }
    });
}

function appendData(data)
{
    PrevTitle.text(data.task.title);
    text = data.task.task_text;
    text = text.split('\n');
    info = '';
    for(i = 0; i < text.length; i++){
        info += '<li class="list-group-item">'+ text[i] +'</li>';
    }
    PrevText.html(info);
    HistoryCard.css('background-image', 'url('+data.task.image+')');
    CreateDate.text('Дата завершения: ' + convertDate(data.task.end_date));
    list = '';
    if(data.notes.length > 0){
        list +='<ul class="list-group">';
        for(i = 0; i < data.notes.length; i++){
            list += '<li class="list-group-item">'+ data.notes[i].text_note +
                    '<br><b> Дата: </b>'+ convertDate(data.notes[i].date_create)+
                    '</li>';
        }
        list += '</ul>';
    }else{
        list = '<h4>Заметок для задачи #'+ GlobalId +' не найдено</h4>';
    }
    NotesList.html(list);
    DeleteItem.attr('href', '/index/Delete/'+ GlobalId);
}

function clearData()
{
    PrevTitle.text('');
    PrevText.html('');
    HistoryCard.css('background-image', '');
    CreateDate.text('');
    NotesList.html('');
    DeleteItem.attr('href', '');
}

function convertDate(unix)
{
    var NewDate = new Date(unix*1000);
    var months = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Июнь','Сентябрь','Октябрь','Ноябрь','Декабрь'];
    var year = NewDate.getFullYear();
    var month = months[NewDate.getMonth()];
    var date = NewDate.getDate();
    var hour = NewDate.getHours();
    var min = NewDate.getMinutes();
    var sec = NewDate.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
}