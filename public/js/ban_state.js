var ban_status = $('#ban_status');
var date_block = $('#date_block');
date_block.hide();

$(document).ready(function(){
    BlockTimeVisibility();
    ban_status.change(BlockTimeVisibility);
    
});
function BlockTimeVisibility () {
    if(ban_status.val() == 1){
        date_block.show('slow');
    }else {
        date_block.hide('slow');
    }
}