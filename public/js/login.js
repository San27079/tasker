//Validation admin input
ValObj = {
    'Status' : {login_stat : 0, pass_stat : 0},
    'Login' : function(){
        var AdminLogin = $('#admin-login');
        if(AdminLogin.val().length >= 3){
            AdminLogin.removeClass('is-invalid');
            AdminLogin.addClass('is-valid');
            ValObj.Status.login_stat = 1;
            ValObj.CheckButton();
        }else{
            AdminLogin.removeClass('is-valid');
            AdminLogin.addClass('is-invalid');
        }
    },
    'Password' : function(){
        var AdminPassword = $('#admin-password');
        if(AdminPassword.val().length >= 3){
            AdminPassword.removeClass('is-invalid');
            AdminPassword.addClass('is-valid');
            ValObj.Status.pass_stat = 1;
            ValObj.CheckButton();
        }else{
            AdminPassword.removeClass('is-valid');
            AdminPassword.addClass('is-invalid');
        }
    },
    'CheckButton' : function(){
        if(ValObj.Status.login_stat && ValObj.Status.pass_stat){
            $('#submit').prop('disabled', false);
        }else{
            $('#submit').prop('disabled', true);
        }
    }
};
$(document).ready(function(){
    $('#admin-password').val("");
    $('#admin-login').val("");
    ValObj.CheckButton();
    $('#admin-password').change(ValObj.Password);
    $('#admin-login').change(ValObj.Login);
});