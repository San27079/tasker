//Init vars
var Subject = $('#subject');
var EmField = $('#email');
var Text = $('#text');
var Name = $('#name');
//Validation feedback input
ValObj = {
    'Status' : {subject_stat : 0, email_stat : 0, text_stat : 0, name_stat : 0},
    'Subject' : function(){
        if(Subject.val().length >= 3){
            Subject.removeClass('is-invalid');
            Subject.addClass('is-valid');
            ValObj.Status.subject_stat = 1;
            ValObj.CheckButton();
        }else{
            Subject.removeClass('is-valid');
            Subject.addClass('is-invalid');
        }
    },
    'Name' : function(){
        if(Name.val().length >= 2){
            Name.removeClass('is-invalid');
            Name.addClass('is-valid');
            ValObj.Status.name_stat = 1;
            ValObj.CheckButton();
        }else{
            Name.removeClass('is-valid');
            Name.addClass('is-invalid');
        }
    },
    'Email' : function(){
        if(/^[0-9a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}$/.test(EmField.val())){
            EmField.removeClass('is-invalid');
            EmField.addClass('is-valid');
            ValObj.Status.email_stat = 1;
            ValObj.CheckButton();
        }else{
            EmField.removeClass('is-invalid');
            EmField.addClass('is-invalid');
        }
    },
    'Text' : function(){
        if(Text.val().length >= 5 && Text.val().length <= 600){
            Text.removeClass('is-invalid');
            Text.addClass('is-valid');
            ValObj.Status.text_stat = 1;
            ValObj.CheckButton();
        }else{
            Text.removeClass('is-valid');
            Text.addClass('is-invalid');
        }
    },
    'CheckButton' : function(){
        if(ValObj.Status.text_stat && ValObj.Status.subject_stat && ValObj.Status.email_stat && ValObj.Status.name_stat){
            $('#submit').prop('disabled', false);
        }else{
            $('#submit').prop('disabled', true);
        }
    }
};

$(document).ready(function(){
    EmField.val("");
    Text.val("");
    Subject.val("");
    Name.val("");
    ValObj.CheckButton();
    Subject.change(ValObj.Subject);
    Text.change(ValObj.Text);
    Name.change(ValObj.Name);
    EmField.change(ValObj.Email);
});