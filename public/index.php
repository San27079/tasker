<?php
/**
 * Created by PhpStorm.
 * User: San27079
 * Date: 14.03.2018
 * Time: 10:29
 */

//Load core classes, or namespace classes
spl_autoload_register(function ($class){
    $class2 = str_replace('\\', DIRECTORY_SEPARATOR, $class);
    require_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."$class2.php";
});

$router = new app\core\Router();
?>